### Hey, I'm Leonardo B. Bussi! 👋


##### Good fleece here

⚓️ 22 years old <br>
🎓 Student Big Data in Agribusiness at Fatec Shunji Nishimura. [gitlab](https://gitlab.com/bdag/) <br>
🇧🇷 Born in Pompeia - São Paulo, Brazil <br>

> Thank you, Lord, for your wonderful love. (Psalm 107: 15).

### Contact 📱

📨 E-mail: leonardobezerrabussy@gmail.com <br>
📞 phone number: ` +55 14 988374395 ` <br>

### Contribution to projects 💻

🗂 CondSystem(Scrum master) [gitlab](https://gitlab.com/bdag/condsystem) <br>
🗂 Projeto-DTX-SPSP(Dev) [gitlab](https://gitlab.com/BDAg/Projeto-DTX-SPSP) <br>
🗂 VisitorCode(Dev) [gitlab](https://gitlab.com/BDAg/qrcodevisits) <br>
🗂 Agrometeorologia(Dev) [gitlab](https://gitlab.com/BDAg/Agrometeorologia) <br>

![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=leonardobussi)

### Find me elsewhere 🌎

🚀 [Site (not portfolio)](https://leonardobussi.github.io) <br>
📸 [Instagram](https://instagram.com/dev_bussi) <br>
💼 [LinkedIn](https://www.linkedin.com/in/leonardobbussi/) <br>


